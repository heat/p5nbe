package bwbr.p5nbe.entidades;

public class Aplicacao {

    String nome;
    
    String versao;
    
    Autor[] autores;

    private Aplicacao(String nome, String versao, Autor[] autores) {
        this.nome = nome;
        this.versao = versao;
        this.autores = autores;
    }

    public String getNome() {
        return nome;
    }

    public String getVersao() {
        return versao;
    }

    public Autor[] getAutores() {
        return autores;
    }
    
    public static Aplicacao getInstance() {
        return new Aplicacao("PsnBe", "0.1", new Autor[]{
            new Autor("Onezino Gabriel", "heat2k4@hotmail.com"),
            new Autor("Reutman Oliveira", "reutman@gmail.com"),
            new Autor("Jesse Fernandes", "jesse.fernandes1@gmail.com")});
    }
    
    public static class Autor {
        String nome;
        String email;

        public Autor(String nome, String email) {
            this.nome = nome;
            this.email = email;
        }

        public String getNome() {
            return nome;
        }

        public String getEmail() {
            return email;
        }
    }
}
