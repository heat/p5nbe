package bwbr.p5nbe.resources;

import bwbr.p5nbe.entidades.Aplicacao;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class PsnBeResource {
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Aplicacao get() {
        return Aplicacao.getInstance();
    }
}
